package u02

object BTrees extends App {

  // A custom and generic binary tree of elements of type A
  sealed trait Tree[A]
  object Tree {
    case class Leaf[A](value: A) extends Tree[A]
    case class Branch[A](left: Tree[A], right: Tree[A]) extends Tree[A]

    def traverse[A, B](t: Tree[A], f: Tree[A] => B, combine: (B, B) => B, orLeaf: A => B): B = t match {
      case Branch(l, r) => combine(f(l), f(r))
      case Leaf(e) => orLeaf(e)
    }

    def size[A](t: Tree[A]): Int =
      traverse[A, Int](t, size(_), (x, y) => x + y, x => 1);

    def find[A](t: Tree[A], elem: A): Boolean =
      _find(elem)(t)

    def _find[A](elem: A)(t: Tree[A]): Boolean =
      traverse[A, Boolean](t, _find(elem), (x, y) => x || y, _ == elem)

    def count[A](t: Tree[A], elem: A): Int =
      _count(elem)(t)

    def _count[A](elem: A)(t: Tree[A]): Int =
      traverse[A, Int](t, _count(elem), (x, y) => x + y, x => if (x == elem) 1 else 0)
  }

  import Tree._
  val tree = Branch(Branch(Leaf(1),Leaf(2)),Leaf(1))
  println(tree, size(tree)) // ..,3
  println( find(tree, 1)) // true
  println( find(tree, 4)) // false
  println( count(tree, 1)) // 2
}
