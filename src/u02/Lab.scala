package u02

object Lab {
  // Exercise 3a
  val parity: Int => String = x =>
    if(x % 2 == 0) "even" else "odd"
  def parity2(x: Int): String =
    if (x % 2 == 0) "even" else "odd"
  // Exercise 3b
  val neg: (String => Boolean) => String => Boolean = x => !x(_)
  def neg2(x: String => Boolean):String => Boolean = !x(_)
  // Exercise 3c
  def negG[A](x: A => Boolean): A => Boolean = !x(_)
  // Exercise 4
  val p1:Int => Int => Int => Boolean = x => y => z => x <= y && y <= z
  val p2:(Int, Int, Int) => Boolean = (x,y,z) => x<=y && y <= z
  def p3(x: Int)(y: Int)(z: Int):Boolean = x <= y && y <= z
  def p4(x: Int, y: Int, z: Int):Boolean = x <= y && y <= z
  // Exercise 5
  def compose[A](f: A => A, g: A => A): A => A = x => f(g(x))
  // Exercise 6
  def fib(n: Int): Int = n match {
    case 0 => 0
    case 1 => 1
    case _ => fib(n - 1) + fib(n - 2)
  }
  // Exercise 7
  sealed trait Shape
  case class Rectangle(width: Double, height: Double) extends Shape
  case class Square(side: Double) extends Shape
  case class Circle(radius: Double) extends Shape

  def perimeter(s: Shape): Double = s match {
    case Circle(r) => 2 * r * Math.PI
    case Rectangle(w, h) => (w + h) * 2
    case Square(s) => s * 4
  }

  def area(s: Shape): Double = s match {
    case Circle(r) => r * r * Math.PI
    case Rectangle(w, h) => w * h
    case Square(s) => s * s
  }
}
