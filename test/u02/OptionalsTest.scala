package u02

import org.junit.jupiter.api.Assertions.{assertEquals, assertFalse, assertTrue}
import org.junit.jupiter.api.Test

class OptionalsTest {
  import Optionals._
  import Option._

  @Test def testFilter() {
    assertEquals(Some(5), filter[Int](Some(5))(_ > 2))
    assertEquals(None(), filter[Int](Some(5))(_ > 8))
  }

  @Test def testMap() {
    assertEquals(Some(true), map[Int, Boolean](Some(5))(_ > 2))
    assertEquals(None(), map[Int, Boolean](None())(_ > 2))
  }

  @Test def testMap2() {
    import Optionals._
    import Option._
    assertEquals(Some(9), map2[Int, Int, Int](Some(5))(Some(4))((x, y) => x + y))
    assertEquals(None(), map2[Int, Int, Int](Some(5))(None())((x, y) => x * y))
  }
}
