package u02

import org.junit.jupiter.api.Assertions.{assertEquals, assertFalse, assertTrue}
import org.junit.jupiter.api.Test

class LabTest {
  import Lab._

  @Test def testNotEmpty() {
    val empty: String => Boolean = _ == ""
    assertTrue(neg (empty)("ciao"))
    assertTrue(neg2(empty)("ciao"))
    assertFalse(negG(empty)(""))
  }

  @Test def testNotEven() {
    val even: Int => Boolean = _ % 2 == 0
    assertTrue(negG(even)(3))
    assertFalse(negG(even)(2))
  }

  @Test def testP() {
    assertTrue(p1(3)(4)(5))
    assertTrue(p2(3,4,5))
    assertTrue(p3(3)(4)(5))
    assertTrue(p4(3,4,5))
  }

  @Test def testCompose() {
    assertEquals(9, compose[Int](_-1,_*2)(5))
  }

  @Test def testPerimeter() {
    val a = 10.0
    val b = 5.0
    assertEquals(2 * a * Math.PI, perimeter(Circle(a)))
    assertEquals((a + b) * 2, perimeter(Rectangle(a,b)))
    assertEquals(a * 4, perimeter(Square(a)))
  }

  @Test def testArea() {
    val a = 10.0
    val b = 5.0
    assertEquals(a * a * Math.PI, area(Circle(a)))
    assertEquals(a * b, area(Rectangle(a, b)))
    assertEquals(a * a, area(Square(a)))
  }
}
